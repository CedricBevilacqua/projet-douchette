﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Douchette
{
    public static class Scan
    {
        /*
         * Detect a bar code, scan it and decode.
        */
        public static string scan()
        {
            String photoCode;
            String code;

            do { } while (recherche() == false);
            photoCode = photographie();
            code = traitement(photoCode);

            return code;
        }

        /*
         * Search a bar code in every picture. In the simulation, search the "START" message.
        */
        private static Boolean recherche()
        {
            String enteredCommand;
            Boolean detectedCode = false;

            Console.WriteLine("Entrez START pour démarrer la saisie.");
            enteredCommand = Console.ReadLine();

            if (enteredCommand == "START")
            {
                detectedCode = true;
            }

            return detectedCode;
        }

        /*
         * Save a photo of the detected bar code. In the simulation, juste save the entered bar code.
        */
        private static String photographie()
        {
            String photoCode;
            Console.WriteLine("Entrez le numéro du code barre : ");
            photoCode = Console.ReadLine();
            return photoCode;
        }

        /*
         * Analyse the photo with the bar code to extract the code in a String. In the simulation, will do nothing.
        */
        private static string traitement(String photoCode)
        {
            String code = photoCode;
            return code;
        }
    }
}
