﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Douchette
{
    class Program
    {
        static void Main(string[] args)
        {
            String code;

            do
            {
                code = Scan.scan(); //Scan bar code
            } while (Verification.verification(code) == false); //Verify bar code
            Sauvegarde.sauvegarde(code); //Save the bar code in an SQLite database
        }
    }
}
