﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace Douchette
{
    public static class Sauvegarde
    {
        /*
         * Save the code in the database.
        */
        public static void sauvegarde(String code)
        {
            SQLiteConnection dbconnector = connexion("Database.db3");
            ajoutBDD(code, "STOCKS", dbconnector);
            dbconnector.Close(); //Close the SqLite database connection
        }

        /*
         * Make a new connection with the SQLite database.
        */
        private static SQLiteConnection connexion(String adresseBDD)
        {
            SQLiteConnection.CreateFile(adresseBDD);
            SQLiteConnection dbconnector;

            dbconnector = new SQLiteConnection("Data Source=" + adresseBDD + ";Version=3;");
            dbconnector.Open();

            return dbconnector;
        }

        /*
         * Add the bar code in the database.
        */
        private static void ajoutBDD(String code, String table, SQLiteConnection dbconnector)
        {
            String sqlcommand;

            try
            {
                sqlcommand = "select * from " + table;
                SQLiteCommand testing_command = new SQLiteCommand(sqlcommand, dbconnector);
                testing_command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlcommand = "create table " + table + " (Type_Product text, Production_Year int, Product_Reference int)";
                SQLiteCommand creation_command = new SQLiteCommand(sqlcommand, dbconnector);
                creation_command.ExecuteNonQuery();
            }

            sqlcommand = "insert into " + table + " (product_reference) values ('" + code + "')";
            SQLiteCommand insertion_command = new SQLiteCommand(sqlcommand, dbconnector);
            insertion_command.ExecuteNonQuery();
        }
    }
}
