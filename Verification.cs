﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Douchette
{
    public static class Verification
    {
        /*
         * Check if the bar code respect the necessary conditions to be valid.
        */
        public static Boolean verification(String code)
        {
            Boolean verified = false;

            if (verifNbPaquets(code) && verifNbCar(code))
            {
                bipSound(1);
                verified = true;
            }

            return verified;
        }

        /*
         * Check if the bar code have the right lenght to be cutted in 3 different packets.
        */
        private static Boolean verifNbPaquets(String code)
        {
            Boolean verified = false;

            if (code.Length == 11)
            {
                verified = true;
            }

            return verified;
        }

        /*
         * Check if the packets' content respect the rules.
        */
        private static Boolean verifNbCar(String code)
        {
            Boolean verified = false;

            if (nbLettres(code) && nbChiffres(code))
            {
                verified = true;
            }

            return verified;
        }

        /*
         * Check if the first caracters are letters.
        */
        private static Boolean nbLettres(String code)
        {
            Boolean verified = false;
            int verifcounter = 0;

            for (int bouclepaquet = 0; bouclepaquet < 3; bouclepaquet++)
            {
                if (Char.IsLetter(code[bouclepaquet]))
                {
                    verifcounter++;
                }
            }

            if (verifcounter == 3)
            {
                verified = true;
            }

            return verified;
        }

        /*
         * Check if the two last packets are composed with 4 numbers.
        */
        private static Boolean nbChiffres(String code)
        {
            Boolean verified = false;
            int verifcounter = 0;

            for (int bouclepaquet = 0; bouclepaquet < 8; bouclepaquet++)
            {
                if (Char.IsNumber(code[bouclepaquet + 3]))
                {
                    verifcounter++;
                }
            }

            if (verifcounter == 8)
            {
                verified = true;
            }

            return verified;
        }

        /*
         * Make a beep.
        */
        private static void bipSound(int nbBips)
        {
            for (int bouclebips = 0; bouclebips < nbBips; bouclebips++)
            {
                Console.WriteLine("BIP");
            }
        }
    }
}
